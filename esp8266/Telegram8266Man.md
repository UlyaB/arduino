# Подключение Arduino UNO к Telegram с помощью WiFi модуля ESP8266
## Подключение модуля
**Схема подключения WiFi [модуля ESP8266](https://aliexpress.ru/item/32341788594.html?algo_pvid=7e7ac9b9-5de7-4305-aef3-fefb5798762a&af=1954_4090455&utm_campaign=1954_4090455&aff_platform=api-new-link-generate&btsid=0b8b036a16047743081324301e0de4&utm_medium=cpa&cn=22rurdh6a1qicogdrgap8qguhvvtt1u5&dp=22rurdh6a1qicogdrgap8qguhvvtt1u5&algo_expid=7e7ac9b9-5de7-4305-aef3-fefb5798762a-2&aff_fcid=9b938804441246d699e8c69b8f00348a-1655223049772-07423-_9GV6Qt&cv=2&ws_ab_test=searchweb0_0%2Csearchweb201602_%2Csearchweb201603_&aff_fsk=_9GV6Qt&sk=_9GV6Qt&aff_trace_key=9b938804441246d699e8c69b8f00348a-1655223049772-07423-_9GV6Qt&terminal_id=a4ae7aad588146c8b71afddfe0ed6179&utm_source=aerkol&utm_content=2) к плате [Arduino UNO](https://aliexpress.ru/item/32831857482.html?algo_pvid=958e1027-72e9-4fd7-bd3f-9e1bdde9469b&item_id=32831857482&af=1954_4090455&utm_campaign=1954_4090455&aff_platform=api-new-link-generate&btsid=21135c3916286154760033502e6086&utm_medium=cpa&sku_id=10000013211724458&cn=22rurdh6cghh853i2pe4fhmhl4ha4vje&dp=22rurdh6cghh853i2pe4fhmhl4ha4vje&ad_pvid=202108101011161635996498338080043328690_1&algo_expid=958e1027-72e9-4fd7-bd3f-9e1bdde9469b-0&aff_fcid=b71094e4f60a48b5b3263001d2852940-1655223136185-01767-_9iCcSp&s=p&cv=2&ws_ab_test=searchweb0_0%2Csearchweb201602_%2Csearchweb201603_&aff_fsk=_9iCcSp&sk=_9iCcSp&aff_trace_key=b71094e4f60a48b5b3263001d2852940-1655223136185-01767-_9iCcSp&terminal_id=a4ae7aad588146c8b71afddfe0ed6179&utm_source=aerkol&utm_content=2):**

[![подключение](https://microkontroller.ru/wp-content/uploads/2022/02/wifitoarduino_jz26ak1qwk.jpg)](https://microkontroller.ru/esp8266-projects/vzaimodejstvie-s-botom-telegram-s-pomoshhyu-esp8266/)

```
Если на WiFi модуле нет названий выходов и они на другой стороне модуля, то выводы располагатся зеркально
```
## Настройка Arduino IDE
Для начала в Arduino IDE нужно открыть ***Файлы/Настройки*** и вставить ссылку в поле: **Дополнительные ссылки для Менеджера плат**

***`ссылка:`***
```
http://arduino.esp8266.com/stable/package_esp8266com_index.json
```
Потом нужно зайти в ***Инструменты/Плата/Менеджер плат***, найти и установить плату esp8266

![Менеджер плат](https://lh3.googleusercontent.com/sUWFOtYoo-MZOr84fgbqr4yf9zDiKXFCXVvkuSjNm8OAFtfUU45Yo8yFJScIHCQpc8TTJg=s1000)

## Подключение библиотек
При выборе платы esp8266 некоторые библиотеки становятся доступными без установки. Нам понадобятся библотеки WiFiClientSecure и ESP8266. Также нужно будет установить библиотеку TelegramBot [с GitHub](https://github.com/CasaJasmina/TelegramBot-Library) или в менеджере библиотек Arduino IDE.
```
Сначала лучше установить библиотеку через Arduino IDE, проверить и посмотреть на ошибки. Если Arduino IDE до сих пор не знает этой библиотеки, попробовать загрузить библиотеку с сайта
```

## **Создание своего Telegram бота**
Сначала нужно зайти в Telegram и найти бот BotFather и ввести команду /newbot. Далее выполнять все по инструкции

![BotFather](https://microkontroller.ru/wp-content/uploads/2022/02/esp124.jpg)

Когда все инструкции выполнены, BotFather выведет Вам API адрес вашего бота (набор букв и символов)

```
Также получить API бота можно, если ввести команду /token после создания бота
```

## **Код**

```
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <TelegramBot.h> // Библиотека для работа с Telegram
#define LED 1
const char* ssid = "xxxxx"; // Имя WiFi сети
const char* password = "*****"; // Пароль от WiFi сети
const char BotToken[] = "***"; // API вашего Телеграм бота
WiFiClientSecure net_ssl;
TelegramBot bot (BotToken, net_ssl);
void setup()
{
  Serial.begin(115200);
  while (!Serial) {}
  delay(3000);
  Serial.print("Connecting Wifi: ");
  Serial.println(ssid);
  while (WiFi.begin(ssid, password) != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println("");
  Serial.println("WiFi connected");
  bot.begin();
  pinMode(LED, OUTPUT);
}
void loop()
{
  message m = bot.getUpdates(); // считываем новые сообщения
  if (m.text.equals("on"))
  {
    digitalWrite(LED, 1);
    bot.sendMessage(m.chat_id, "The Led is now ON");
  }
  else if (m.text.equals("off"))
  {
    digitalWrite(LED, 0);
    bot.sendMessage(m.chat_id, "The Led is now OFF");
  }
}
```

#### ***`Другие источники:`***
https://microkontroller.ru/esp8266-projects/vzaimodejstvie-s-botom-telegram-s-pomoshhyu-esp8266/

https://the-robot.ru/study/arduino-i-telegram-urok-1-arduino-ide-bot-telegram/

void setup() {
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
}

void loop() {
  for (int i = 8; i < 12; ++i) {
    digitalWrite(i, !digitalRead(i));
    delay(200);
  }
  for (int i = 12; i > 8; --i) {
    digitalWrite(i, !digitalRead(i));
    delay(200);
  }
}

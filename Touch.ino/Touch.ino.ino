void setup() {
  Serial.begin(9600);
  pinMode(13, OUTPUT);
  pinMode(9, OUTPUT); 
}

void loop() {
  int TOUCH_ONE = digitalRead(3);
  //Serial.println(TOUCH);
  if (TOUCH_ONE == true) {
    digitalWrite(13, HIGH);
  }
  else {
    digitalWrite(13, LOW);
  }
  int TOUCH_TWO = digitalRead(4);
  if (TOUCH_TWO == true) {
    digitalWrite(9, HIGH);
  }
  else {
    digitalWrite(9, LOW);
  }
  int TOUCH_THREE = digitalRead(5);
  if (TOUCH_THREE == true) {
    digitalWrite(9, HIGH);
    digitalWrite(13, HIGH);
  }
  else {
    digitalWrite(9, LOW);
    digitalWrite(13, LOW);
  }
}
